const path = require(`path`);

/**
 * экспортируемая функция, которая перезапишет существующую по умолчанию
 * и будет вызвана для генерации страниц
 */
exports.createPages = ({ graphql, actions }) => {
    /**
     * получаем метод для создания страницы из экшенов
     * чтобы избежать лишних импортов и сохранять контекст
     * страницы и функции
     */
    const { createPage } = actions;
    return graphql(`
    {
      allContentfulBlogPost(filter: {node_locale: {eq: "en-US"}}) {
        edges {
          node {
            contentful_id
            title
            postImage {
              file {
                url
              }
            }
            postContent {
              childMarkdownRemark {
                html
              }
            }
            postAuthor
            createdAt(fromNow: true)  
          }
        }
      }
    }
  `).then(({ data: { allContentfulBlogPost: { edges } } }) => {
        /**
         * для каждого из элементов из ответа
         * вызываем createPage() функцию и передаём
         * внутрь данные с помощью контекста
         */
        edges.forEach(({ node }) => {
            createPage({
                path: node.contentful_id,
                component: path.resolve(`./src/templates/index.js`),
                context: {
                    slug: node.contentful_id
                }
            });
        });
    });
};
