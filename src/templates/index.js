import React from "react";
import { graphql } from "gatsby";
import styled from "styled-components";

import Layout from "../components/layout";

const PostImg = styled.img`
    width: 100%
`;

const PostFooter = styled.div`
    display: flex;
    justify-content: space-between;
`;

export default ({
    data: {
        allContentfulBlogPost: {
            edges: [
                {
                    node: {
                        contentful_id,
                        title,
                        postImage: {
                            file: {
                                url
                            }
                        },
                        postContent: {
                            childMarkdownRemark: { html }
                        },
                        postAuthor,
                        createdAt
                    }
                }
            ]
        }
    }
}) => {
    return (
        <Layout>
            <h1>{title}</h1>
            <PostImg src={url} alt={title}/>
            <div dangerouslySetInnerHTML={{ __html: html }} />
            <PostFooter>
                <p>{postAuthor}</p>
                <p>{createdAt}</p>
            </PostFooter>
        </Layout>
    );
};

export const query = graphql`
  query($slug: String!) {
    allContentfulBlogPost(filter: { contentful_id: { eq: $slug } }) {
        edges {
          node {
            contentful_id
            title
            postImage {
              file {
                url
              }
            }
            postContent {
              childMarkdownRemark {
                html
              }
            }
            postAuthor
            createdAt(fromNow: true)  
          }
        }
      }
  }
`;
