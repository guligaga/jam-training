import React from "react";
import { Link, StaticQuery, graphql } from "gatsby";
import styled from "styled-components";

import { colors } from "../utils/vars";

const Sidebar = styled.section`
    position: sticky;
    top: 0;
    left: 0;
    width: 20%;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    background-color: ${colors.second};
    color: ${colors.textMain};
  `;

const navItem = `
    display: flex;
    align-items: center;
    margin: 0 1em 0 2em;
    padding: 0.5em 0;
    border-bottom: 0.05em solid ${colors.main50};
    postion: relative;
    color: ${colors.textBody};
    text-decoration: none;

    &:before {
      content: '';
      transition: 0.5s;
      width: 0.5em;
      height: 0.5em;
      position: absolute;
      left: 0.8em;
      border-radius: 50%;
      display: block;
      background-color: ${colors.main};
      transform: scale(0);
    }

    &:last-child {
      border-bottom: none;
    }

    &:hover {
      color: ${colors.main};
    }
  `;

export default () => (
    <StaticQuery
        query={graphql`
        {
            allContentfulBlogPost(sort: {fields: createdAt, order: DESC}, filter: {node_locale: {eq: "en-US"}}) {
            edges {
                node {
                  contentful_id
                  title
                  createdAt(fromNow: true)
                }
            }
          }
        }
      `}
        render={({ allContentfulBlogPost: { edges } }) => (
            <Sidebar>
                {edges.map(({ node: { title, contentful_id } }) => (
                    <Link to={contentful_id} key={contentful_id} css={navItem}>
                        {title}
                    </Link>
                ))}
            </Sidebar>
        )}
    />
);
