import React from "react";

import Header from "./header";
import Sidebar from "./sidebar";
import styled from "styled-components";

const Main = styled.main`
    margin: 30px auto;
    width: 100%;
    max-width: 650px;
`;


export default ({ children }) => (
    <>
        <Header />
        <Sidebar />
        <Main>
            {children}
        </Main>
    </>
);
